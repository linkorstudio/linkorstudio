<?php
    $to = "sales@linkorstudio.com";

    $a = $_POST['action'];
    if ($a == 'files') {

        $files = $_POST['files'];
        $message = $_POST['message'];
        $subject = $_POST['subject'];
        // Creatign a zip arhive
        $zip = new ZipArchive();
        $filename = "files/client-files-".date('mdhis').".zip";

        if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
            exit("Невозможно открыть <$filename>\n");
        }

        foreach ($files as $key => $value) {
            if ($value !== 'undefined') {
                $zip->addFile($value['path'], $value['name']);    
            }
        }

        $zip->close();

        // deleting 
        foreach ($files as $key => $value) {
            unlink($value['path']);
            rmdir($value['pathdir']);    
        }

        $message .= '
            <br> 
            Link on this file to download: 
            <a href="http://linkorstudio.com/'.$filename.'">http://linkorstudio.com/'.$filename.'</a>;
            <br> 
            File have been uploaded '.date('d-m-Y').' at '.date('H:i:s').'
            <br>
            You should write responce not after than 24 hour!
            <br> 
            Just keep swimming and have a nice life!)'; 

        // genering email
        $file = $filename;
        $file_size = filesize($file);
        $handle = fopen($file, "r");
        $content = fread($handle, $file_size);
        fclose($handle);
        $content = chunk_split(base64_encode($content));

        // a random hash will be necessary to send mixed content
        $separator = md5(time());

        // carriage return type (we use a PHP end of line constant)
        $eol = PHP_EOL;

        // main header (multipart mandatory)
        $headers = "From: name <sales@linkorstudio.com>" . $eol;
        $headers .= "MIME-Version: 1.0" . $eol;
        $headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"" . $eol . $eol;
        $headers .= "Content-Transfer-Encoding: 7bit" . $eol;
        $headers .= "This is a MIME encoded message." . $eol . $eol;

        // message
        $headers .= "--" . $separator . $eol;
        $headers .= "Content-Type: text/html; charset=\"iso-8859-1\"" . $eol;
        $headers .= "Content-Transfer-Encoding: 8bit" . $eol . $eol;
        $headers .= $message . $eol . $eol;

        // attachment
        $headers .= "--" . $separator . $eol;
        $headers .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"" . $eol;
        $headers .= "Content-Transfer-Encoding: base64" . $eol;
        $headers .= "Content-Disposition: attachment" . $eol . $eol;
        $headers .= $content . $eol . $eol;
        $headers .= "--" . $separator . "--";

        //SEND Mail
        echo mail($to, $subject, "", $headers);

    } else {
        $from = $_REQUEST['name'];
        $subject = $_REQUEST['subject'];
        $name = $_REQUEST['name'];
        $headers = "From: $from";

        $fields = array();
        $fields{"name"} = "name";
        $fields{"email"} = "email";
        $fields{"subject"} = "subject";
        $fields{"message"} = "message";

        $body = "Here is what was sent:\n\n"; foreach($fields as $a => $b){   $body .= sprintf("%20s: %s\n",$b,$_REQUEST[$a]); }

        $send = mail($to, $subject, $body, $headers);
    }
?>
