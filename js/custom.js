/* ========================================================================= */
/*	Preloader
/* ========================================================================= */

jQuery(window).load(function(){

	$("#preloader").fadeOut("slow");

});


$(document).ready(function(){
    
    $('.cd-nav-trigger').on('click', function(event){
        $('.cd-3d-nav-container').toggleClass('nav-is-visible');
        $('.main').toggleClass('nav-is-visible');
        $('body').toggleClass('nav-is-visible');
    });
//    $('.cd-3d-nav a').on('click', function(event) {
//        $('.cd-3d-nav-container').toggleClass('nav-is-visible');
//        $('.main').toggleClass('nav-is-visible');
//        $('body').toggleClass('nav-is-visible');
//    });
    
	/* ========================================================================= */
	/*	Menu item highlighting
	/* ========================================================================= */

	jQuery('.nav').singlePageNav({
		offset: jQuery('#nav').outerHeight(),
		filter: ':not(.external)',
		speed: 1200,
		currentClass: 'current',
		easing: 'easeInOutExpo',
		updateHash: true,
		beforeStart: function() {
			console.log('begin scrolling');
		},
		onComplete: function() {
			console.log('done scrolling');
      
		}
	});
	
    $(window).scroll(function () {
        if ($(window).scrollTop() > 400) {
            $("#navigation").css("background-color","#222");
            // $(".navbar-brand").css("display","block");
            $(".navbar-brand").addClass("visible");
        } else {
            $("#navigation").css("background-color",'transparent'); //rgba(16, 22, 54, 0)
            // $(".navbar-brand").css("display","none");
            $(".navbar-brand").removeClass("visible");
        }
    });
	
	/* ========================================================================= */
	/*	Fix Slider Height
	/* ========================================================================= */	

	var slideHeight = $(window).height();
	
	$('#slider, .carousel.slide, .carousel-inner, .carousel-inner .item').css('height',slideHeight);

	$(window).resize(function(){'use strict',
		$('#slider, .carousel.slide, .carousel-inner, .carousel-inner .item').css('height',slideHeight);
	});
	
	
	/* ========================================================================= */
	/*	Portfolio Filtering
	/* ========================================================================= */	
	
	
    // portfolio filtering

    $(".project-wrapper").mixItUp();
	

	
	/* ========================================================================= */
	/*	Parallax
	/* ========================================================================= */	
	
	$('#facts').parallax("50%", 0.3);
	
	
	/* ========================================================================= */
	/*	Back to Top
	/* ========================================================================= */
	
	
    $(window).scroll(function () {
        if ($(window).scrollTop() > 400) {
            $("#back-top").fadeIn(200)
        } else {
            $("#back-top").fadeOut(200)
        }
    });
    $("#back-top, .navbar-brand").click(function () {
        $("html, body").stop().animate({
            scrollTop: 0
        }, 1500, "easeInOutExpo")
    });


    /* ========================================================================= */
    /*  Scroll To Anchor
    /* ========================================================================= */
    // $('.cd-3d-nav a').click(function() {
    //   var anchor = jQuery(this).attr('href');
    //   console.log(anchor);
    //   $('html, body').animate({
    //       scrollTop: anchor.offset().top
    //   }, 2000);
    // });
	
});


// ==========  START GOOGLE MAP ========== //
// function initialize() {
//     var myLatLng = new google.maps.LatLng(22.402789, 91.822156);

//     var mapOptions = {
//         zoom: 14,
//         center: myLatLng,
//         disableDefaultUI: true,
//         scrollwheel: false,
//         navigationControl: false,
//         mapTypeControl: false,
//         scaleControl: false,
//         draggable: false,
//         mapTypeControlOptions: {
//             mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'roadatlas']
//         }
//     };

//     var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);


//     var marker = new google.maps.Marker({
//         position: myLatLng,
//         map: map,
//         icon: 'img/location-icon.png',
//         title: '',
//     });

// }


// Bottom of page
jQuery(document).ready(function() {
    var wow = new WOW({
        boxClass: 'wow', // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset: 120, // distance to the element when triggering the animation (default is 0)
        mobile: false, // trigger animations on mobile devices (default is true)
        live: true // act on asynchronously loaded content (default is true)
    });
    wow.init();
});

// Scrolliing
jQuery(document).ready(function($) {
    // browser window scroll (in pixels) after which the "back to top" link is shown
    var offset = 300,
        //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
        offset_opacity = 1200,
        //duration of the top scrolling animation (in ms)
        scroll_top_duration = 700,
        //grab the "back to top" link
        $back_to_top = $('.cd-top');

    //hide or show the "back to top" link
    $(window).scroll(function() {
        ($(this).scrollTop() > offset) ? $back_to_top.addClass('cd-is-visible'): $back_to_top.removeClass('cd-is-visible cd-fade-out');
        if ($(this).scrollTop() > offset_opacity) {
            $back_to_top.addClass('cd-fade-out');
        }
    });

    //smooth scroll to top
    $back_to_top.on('click', function(event) {
        event.preventDefault();
        $('body,html').animate({
            scrollTop: 0,
        }, scroll_top_duration);
    });
});


 /* ========================================================================= */
/*	Contact Form
/* ========================================================================= */
$(function() {
    $('#contact-form').validate({
        rules: {
            name: {
                required: true,
                minlength: 2
            },
            email: {
                required: true,
                email: true
            },
            message: {
                required: true
            }
        },
        messages: {
            name: {
                required: "Да ну, неужели у Вас нет имени?",
                minlength: "Ваше имя должно состоять как минимум из двух символов."
            },
            email: {
                required: "Нет email - нет сообщения."
            },
            message: {
                required: "Эмм... Понимаете, Вы должны здесь что-то написать, чтобы отправить сообщение.",
                minlength: "И это всё? Действительно?"
            }
        },
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                type: "POST",
                data: $(form).serialize(),
                url: "process.php",
                success: function() {
                    $('#contact-form :input').attr('disabled', 'disabled');
                    $('#contact-form').fadeTo("slow", 0.15, function() {
                        $(this).find(':input').attr('disabled', 'disabled');
                        $(this).find('label').css('cursor', 'default');
                        $('#success').fadeIn();
                    });
                },
                error: function() {
                    $('#contact-form').fadeTo("slow", 0.15, function() {
                        $('#error').fadeIn();
                    });
                }
            });
        }
    });
});