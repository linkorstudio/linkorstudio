var isModalShow = false;
var popupContent;
jQuery('.ls-popup').click(function(event){
    popupContent = jQuery(this).find('.ls-modal-content').html(); // запоминаем контент элемента, по которому кликнули
    jQuery('#ls-modal-content').html(popupContent); // записываем контент в попап окно
   
    var cont = jQuery('#ls-modal-content');
    BackgroundCheck.init({
        targets:  cont.children('div'),
        images:  cont.find('.ls-slides').children('img')
    });


    event.preventDefault(); // выключаем стандартную роль элемента
    jQuery('#ls-overlay').fadeIn(400, // сначала плавно показываем темную подложку
			 	function(){ // после выполнения предъидущей анимации
					jQuery('#ls-modal-form') 
						.css('display', 'block') // убираем у модального окна display: none;
						.animate({opacity: 1}, 200); // плавно прибавляем прозрачность одновременно со съезжанием вниз
					jQuery("html,body").css("overflow","hidden");	//отключаем скролл страницы	
					isModalShow = true;
			});
   	/* Используем в случае подгрузки данных */
    // postLink = jQuery(this).data('link');
    // jQuery.post(
    //     postLink, function (data) {
    //         jQuery('#ls-modal-form').find('#ls-modal-content').html(data);
    //     }
    // );


	// Отложенная подгрузка изображений
	jQuery.each(cont.find('.ls-slides').children('img'), function(i,v) {
		var src = jQuery(this).data('src');
		jQuery(this).attr('src', src);
	});
});

/* Закрытие модального окна, тут делаем то же самое но в обратном порядке */
jQuery('#ls-modal-close, #ls-overlay').click( function(){ // ловим клик по крестику или подложке
	closeModal();
});

/* Закрытие модального окна, при нажатии ESC */ 
jQuery(document).keydown(function(e){
	if (e.keyCode==27) { closeModal(); }
});

/* Закрытие модального окна при клике за его пределы */ 
jQuery('body').click(function(event) {
	var target = jQuery(event.target);
	if (target.attr('id') == 'ls-modal-form' && isModalShow) 
		closeModal();
	else 
		return true;
});

function closeModal() {
	// Испльзуем в случае подгрузки данных
	//jQuery('#ls-modal-form').find('#ls-modal-content').html('');
	jQuery('#ls-modal-form')
		.animate({opacity: 0}, 200,  // плавно меняем прозрачность на 0 и одновременно двигаем окно вверх
			function(){ // после анимации
				jQuery(this).css('display', 'none'); // делаем ему display: none;
				jQuery('#ls-overlay').fadeOut(400); // скрываем подложку
			}
		);
	jQuery("html,body").css("overflow","auto");	//включаем скролл страницы
	isModalShow = false;
}
