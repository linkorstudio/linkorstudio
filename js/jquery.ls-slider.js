// (function($) {
	var sName = $('.ls-slider'),
		onParent = $('#ls-modal-form'),
		sliders = [];
	
	$.each(sName, function (ind,v) {
		var n = 0, 
			dots = '',
			sName = $(this),
			slides = sName.children('img'),
			amount = slides.length;
		
		/* Creating element */
		var id = 'slider-'+(ind+1);
		sName.attr('id', id);
		sName.wrapInner('<div class="ls-slides"></div>');

		if (amount > 1) {
			sName.prepend('<div class="ls-lft"></div>');
			sName.append('<div class="ls-rgt"></div>');
			/* Создаем маркеры */
			for (var i = 0; i < amount; i++) {
				(i==n) ? act = 'ls-active ' : act = '';
				dots += '<li class="'+act+'" id="slide-'+(i+1)+'">\
							<button type="button">'+(i+1)+'</button>\
						</li>';
			}
			sName.append('<ul class="ls-dots">'+dots+'</ul>');
		}

		slides.hide(); // Скрываем все изображения
		$(slides[n]).show(); // Показываем первое изображение

		sliders.push({
			'id' : id,
			'n' : 0
		});
	});

	// Если есть комбинация on сработает она
	onParent.on('click', '.ls-rgt, .ls-lft, button', function() {
		id = $(this).closest('.ls-slider').attr('id');
		slideAct( this, onParent, id);
		BackgroundCheck.refresh();
	});

	onParent.on("swipeleft", 'img', function(){
	  onParent.find('.ls-lft').trigger('click');
	});

	onParent.on("swiperight", 'img', function(){
	  onParent.find('.ls-rgt').trigger('click');
	});

	window.addEventListener("keydown", function (event) {
		switch(event.keyCode) {
			case 37:
				onParent.find('.ls-lft').trigger('click');
				break;
			case 39:
				onParent.find('.ls-rgt').trigger('click');
				break;
		}
	});

	// Описание
	onParent.on('click', '.info span', function() {
		var el = jQuery(this).data('type');
		var content = jQuery(this).closest('.content');
		content.find('.is-active').removeClass('is-active');
		jQuery(this).addClass('is-active');
		content.find('.desc').hide();
		content.find('.'+el).show();
	});

	// Если нет, простой клик
	// $('.ls-rgt, .ls-lft, button', sName).click( function() {
	//	id = $(this).parent().attr('id');
	// 	slideAct( this, id );
	// });

	function slideAct($this, context, id) {
		if (context !== undefined) {
			$this = context.find($this);
			slides = context.find('.ls-slides').children('img');
			sName = context;
		} else {
			$this = $($this);
			slides = sName.find('.ls-slides').children('img');
		}

		n = sliders[id.split('-')[1]-1]['n'];

		amount = slides.length;

		$(slides[n]).hide();
		
		/* Вправо или влево? */
		if ( $this.hasClass('ls-rgt') ) {
			n++; if(n == amount) n=0; 
		} else if ($this.attr('type') == 'button') {
			n = $this.parent().attr('id').split('-')[1] - 1;
		} else {
			n--; if(n < 0) n=amount-1;
		}
	
		$(slides[n]).show();
		// Если изображение маленькое даем ему класс выравнивания по центру
		$(slides[n]).parent().parent().animate({ scrollTop: 0 }, 0);
		
		$('.ls-dots', sName).find('li').removeClass('ls-active');
		$('#slide-'+(n+1), sName).addClass('ls-active'); // Ставим нужный
		
		// clearInterval(intervalID); // Пока клацаем нет слайдинга
		// setTimeout(auto(), interval); // А пото опять есть
		sliders[id.split('-')[1]-1]['n'] = n;
	}

	// // Автоматическое переключение
	// function auto(){
	// 	if(interval > 0) {
	// 		intervalID = setInterval(function(){$('#to_right').trigger('click')}, interval); 
	// 	}
	// }
	// // Запуск слайдера
	// auto();
// })(jQuery);